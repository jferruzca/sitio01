<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/ejemplo/insertar", name="insertaProductoEjemplo")
     */
    public function insertaProductoEjemplo(){
        
        $product = new Product();
        $product->setName("Agua Bonafont");
        $product->setPrice(12.5);
        $product->setDescription("Esta es una descripción larga del producto");
        $product->setShortDescription("Descripcion corta del producto.");

        $insertar = $this->getDoctrine()->getManager();
        $insertar->persist($product); //Le dice a doctrine que desea guardar el producto eventualmente, sin querys
        $insertar->flush(); //Inserta en la base de datos, Insert Query

        return new Response('Producto Guardado con el id:'.$product->getId());
    }

    /**
     * @Route("/ejemplo/buscar/{id}", name="buscaProductoEjemplo")
     */
    public function buscaProductoEjemplo($id){
        $product = $this->getDoctrine()->getRepository('AppBundle:Product')->find($id);

        if(!$product){
            //throw $this->createNotFoundException('No se encontró el ID: '.$id);
            return new Response("No existe el ID: ".$id);
        }else{
            return new Response("Si existe el Producto con el ID:".$id." Nombre: ".$product->getName());
        }
    }

    /**
     * @Route("/ejemplo/actualiza/{id}", name="actualizaProductoEjemplo")
     */
    public function actualizaProductoEjemplo($id){
        $actualiza = $this->getDoctrine()->getManager();
        $product = $actualiza->getRepository('AppBundle:Product')->find($id);

        if(!$product){
            //throw $this->createNotFoundException('No se encontró el ID: '.$id);
            return new Response("No existe el ID: ".$id);
        }else{
            $product->setDescription('Actualizamos Descripcion');
            $actualiza->flush();

            return new Response("Se actualizó el ID: ".$id);
        }
    }

    /**
     * @Route("/ejemplo/elimina/{id}", name="eliminaProductoEjemplo")
     */
    public function eliminaProductoEjemplo($id){
        $elimina = $this->getDoctrine()->getManager();
        $product = $elimina->getRepository('AppBundle:Product')->find($id);

        if(!$product){
            //throw $this->createNotFoundException('No se encontró el ID: '.$id);
            return new Response("No existe el ID: ".$id);
        }else{
            $elimina->remove($product);
            $elimina->flush();

            return new Response("Se eliminó el ID: ".$id);
        }
    }    

}
